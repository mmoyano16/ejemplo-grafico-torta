﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EjemploGrafico.Controles
{
    public partial class GraficoTorta : System.Web.UI.UserControl
    {
        // props
        // ----------------------------------------------
        private GraficoTortaDataset dataset = null;

        // metodos
        // ----------------------------------------------

        public void LimpiarDatos()
        {
            this.dataset = new GraficoTortaDataset();
        }

        public void AgregarDatavalue(string label, int value)
        {
            if (this.dataset == null)
                LimpiarDatos();

            this.dataset.AgregarDatavalue(label, value);
        }

        public override void DataBind()
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(this.dataset);
            hidDataset.Value = json;
        }

        // eventos
        // ----------------------------------------------


        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }

    public class GraficoTortaDataset
    {
        public List<GraficoTortaDatavalue> Data { get; set; }

        public void AgregarDatavalue(string label, int value)
        {
            if (Data == null)
                Data = new List<GraficoTortaDatavalue>();
            Data.Add(new GraficoTortaDatavalue() {
                Label = label,
                Value = value
            });
        }
    }

    public class GraficoTortaDatavalue
    {
        public string Label { get; set; }
        public int Value { get; set; }
    }

}